FROM nginx:latest

RUN rm -rf /etc/nginx/conf.d/*

COPY public /usr/share/nginx/html

COPY APIdata /usr/share/nginx/html/APIdata

COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
