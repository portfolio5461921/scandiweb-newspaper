# Newspaper App

Šajā repozitorijā atrodas laikraksta lietotne, kas demonstrē vienkāršu ziņu vietni. HTML tiek ģenerēts dinamiski, izmantojot JavaScript, un saturs tiek iegūts no *Current News* API un saglabāts lokāli API ierobežojumu dēļ.

🌐 Lietotne ir pieejama šeit: [Newspaper App](http://ec2-3-95-167-155.compute-1.amazonaws.com/)

## Funkcijas

* **Ziņu vietne:** Lietotne parāda ziņu vietni ar dinamiski ģenerētu HTML saturu.
* **Current News API integrācija:** Lietotne iegūst ziņu datus no *Current News* API, lai aizpildītu vietni ar aktuālām ziņu rakstiem. ❗API ierobežojumu dēļ iegūtais ziņu saturs tiek saglabāts lokāli, lai tas būtu pieejams arī bez aktīvas API savienojuma.
* **Cilvēkam saprotams datumu formāts:** Ziņu rakstu datumus pārveido cilvēkam saprotamā formātā. Piemēram, no "2023-02-20 06:05:23 +0000" uz "pirms 3 minūtēm".
* **Preču tirgus pārskats:** Lapas apakšējā daļā ir redzams preču tirgus pārskats. Tas parāda nosaukumu, mērvienību, intervālu, cenu izmaiņas procentos (krāsu izmaiņas, atkarībā no palielināšanās vai samazināšanās) un diagrammu.

## Izmantotās tehnoloģijas

* **Frontend:** JavaScript
* **API:** Current News, Alpha Vantage
