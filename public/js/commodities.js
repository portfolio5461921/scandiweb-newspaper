const commodities = [
  "brent",
  "natural_gas",
  "copper",
  "aluminum",
  "coffee",
  "wheat",
  "corn",
  "cotton",
  "sugar",
];

function fetchData(commodity) {
  return fetch(`../APIdata/${commodity}.json`)
    .then((response) => response.json())
    .then((data) => {
      let current = data.data[0].value;
      let last = data.data[1].value;

      let color = "#ADADAD";

      if (current > last) {
        color = "#79C2BA";
      } else {
        color = "#C27881";
      }

      function capitalizeWords(str) {
        return str
          .split(" ")
          .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
          .join(" ");
      }

      let change = (((current - last) / current) * 100).toFixed(2);
      let name = capitalizeWords(data.name);
      let unit = capitalizeWords(data.unit);
      let interval = capitalizeWords(data.interval);

      if (change == 0.0) {
        color = "#ADADAD";
      }

      let str =
        "<div class='content brent'><div class='description'><span class='name open-sans-normal'>" +
        name +
        "</span><span class='unit open-sans-light'>" +
        unit +
        "</span><span class='interval open-sans-light'>" +
        interval +
        "</span>";

      str +=
        "<span class='change open-sans-normal' style=" +
        "color:" +
        color +
        ">" +
        change +
        "%</span></div>";

      let points = "";

      const values = data.data
        .filter((datum) => datum.value !== "." && datum.value !== "")
        .map((datum) => datum.value);

      let x = 100 / values.length;

      const maxValue = Math.max(...values);

      for (let i = values.length - 1, j = 0; i >= 0; i--, j++) {
        const currentValue = values[i];
        let pointX = x * j;
        let pointY = (1 - currentValue / maxValue) * 100;

        points += `${pointX}% ${pointY}%, `;
      }

      str +=
        "<div class='area-chart' style='clip-path: " +
        `polygon(0% 100%, ${points} 100% 100%);` +
        "'>";

      str += "</div></div>";

      return str;
    });
}

const container = document.querySelector(".commodities__container");

Promise.all(commodities.map(fetchData))
  .then((results) => {
    container.innerHTML = results.join("");
  })
  .catch((error) => {
    console.error(error);
  });
