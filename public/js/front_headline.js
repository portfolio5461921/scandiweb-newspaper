fetch("/api/latest_news.json")
  .then((response) => response.json())
  .then((data) => {
    function formatDate(dateString) {
      const now = new Date();
      const date = new Date(dateString.replace(/-/g, "/"));
      const diff = (now - date) / 1000;

      if (diff < 60) {
        return "Just now";
      } else if (diff < 60 * 60) {
        const minutes = Math.floor(diff / 60);
        return `${minutes} minute${minutes !== 1 ? "s" : ""} ago`;
      } else if (diff < 24 * 60 * 60) {
        const hours = Math.floor(diff / (60 * 60));
        return `${hours} hour${hours !== 1 ? "s" : ""} ago`;
      } else if (diff < 7 * 24 * 60 * 60) {
        const days = Math.floor(diff / (24 * 60 * 60));
        return `${days} day${days !== 1 ? "s" : ""} ago`;
      } else {
        return date.toLocaleDateString();
      }
    }

    let title = data.news[0].title;
    const dateString = data.news[0].published;
    const formattedDate = formatDate(dateString);

    if (title.includes("(arXiv")) {
      const originalString = data.news[0].title.split("(");
      const stopIndex = originalString.indexOf();
      title = originalString.slice(0, stopIndex);
    }

    let category = data.news[0].category[0];
    for (let i = 1; i < data.news[0].category.length; i++) {
      category += ", ";
      category += data.news[i].category[0];
    }

    const frontHeadline = document.querySelector(
      ".front-headlines__front-headline"
    );

    if (data.news[0].image === "None" || data.news[0].image === "") {
      frontHeadline.innerHTML +=
        "<div class='image'></div>" +
        "<span class='date open-sans-light'>" +
        formattedDate +
        " in the " +
        category +
        "</span> " +
        "<a href=" +
        data.news[0].url +
        " target='_blank' class='title noticia-bold'>" +
        title +
        "</a>";
    } else {
      frontHeadline.innerHTML +=
        "<img src=" +
        data.news[0].image +
        " class='image'> " +
        "<span class='date open-sans-normal'>" +
        formattedDate +
        " in the " +
        category +
        "</span>" +
        "<a href=" +
        data.news[0].url +
        " target='_blank' class='title noticia-bold'> " +
        title +
        "</a>";
    }
  });
