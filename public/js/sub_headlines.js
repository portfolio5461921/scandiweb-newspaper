fetch("/api/latest_news.json").then((response) =>
  response.json().then((data) => {
    function formatDate(dateString) {
      const now = new Date();
      const date = new Date(dateString.replace(/-/g, "/"));
      const diff = (now - date) / 1000;

      if (diff < 60) {
        return "Just now";
      } else if (diff < 60 * 60) {
        const minutes = Math.floor(diff / 60);
        return `${minutes} minute${minutes !== 1 ? "s" : ""} ago`;
      } else if (diff < 24 * 60 * 60) {
        const hours = Math.floor(diff / (60 * 60));
        return `${hours} hour${hours !== 1 ? "s" : ""} ago`;
      } else if (diff < 7 * 24 * 60 * 60) {
        const days = Math.floor(diff / (24 * 60 * 60));
        return `${days} day${days !== 1 ? "s" : ""} ago`;
      } else {
        return date.toLocaleDateString();
      }
    }

    for (let i = 1; i < 7; i++) {
      let title = data.news[i].title;
      const dateString = data.news[i].published;
      const formattedDate = formatDate(dateString);

      if (title.includes("(arXiv")) {
        const originalString = data.news[i].title.split("(");
        const stopIndex = originalString.indexOf();
        title = originalString.slice(0, stopIndex);
      }

      let category = String(data.news[i].category).replaceAll(",", ", ");

      if (data.news[i].image === "None" || data.news[i].image === "") {
        document.querySelector(".front-headlines__sub-headlines").innerHTML +=
          "<div class='content'>" +
          "<div class='image'></div>" +
          "<span class='date open-sans-light'>" +
          formattedDate +
          " in the " +
          category +
          "</span> " +
          "<a href=" +
          data.news[i].url +
          " target='_blank' class='title noticia-bold'>" +
          title +
          "</a>" +
          "</div>";
      } else {
        document.querySelector(".front-headlines__sub-headlines").innerHTML +=
          "<div class='content'>" +
          "<img src=" +
          data.news[i].image +
          " class='image'>" +
          "<span class='date open-sans-light'>" +
          formattedDate +
          " in the " +
          category +
          "</span> " +
          "<a href=" +
          data.news[i].url +
          " target='_blank' class='title noticia-bold'>" +
          title +
          "</a>" +
          "</div>";
      }
    }
  })
);
